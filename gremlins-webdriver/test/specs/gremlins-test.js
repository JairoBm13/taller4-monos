function loadScript(callback) {
  var s = document.createElement('script');
  s.src = 'https://rawgithub.com/marmelab/gremlins.js/master/gremlins.min.js';
  if (s.addEventListener) {
    s.addEventListener('load', callback, false);
  } else if (s.readyState) {
    s.onreadystatechange = callback;
  }
  document.body.appendChild(s);
}

function unleashGremlins(ttl, callback) {
  function stop() {
    horde.stop();
    callback();
  }
  var fillerGremlin = window.gremlins.species.formFiller();
  fillerGremlin.canFillElement(function(element) { 
    if(element == null) return false;
    var tagName = element.tagName;
    if(tagName== null) return false;
    if (tagName.toLowerCase() === 'textarea') {
      return true;
    } 
    if(tagName.toLowerCase()==='input') {
      
      var type = element.getAttribute('type').toLowerCase(),
      //if any of these input types is not supported by a browser, it will behave as input type text.
      inputTypes = 
        ['text', 'password', 'number', 'email', 'tel', 
          'url', 'search', 'date', 'datetime', 
          'datetime-local', 'time', 'month', 'week'];
      return inputTypes.indexOf(type) >= 0;
    }
    else return false;
  }); 
  var clickerGremlin = window.gremlins.species.clicker();
  clickerGremlin.canClick(function(element){
    if(element == null) return false;
    var tagName = element.tagName;
    if(tagName== null) return false;
    if (tagName.toLowerCase() === 'a' || tagName.toLowerCase() === 'button') {
      return true;
    }
    else return false;
  });
  var horde = window.gremlins.createHorde()
    .gremlin(fillerGremlin)
    .gremlin(clickerGremlin)
    .gremlin(function() {
      window.$ = function() {};
  });

  horde.strategy(gremlins.strategies.distribution()
  .delay(50) // wait 50 ms between each action
  .distribution([0.4, 0.6]) // the first three gremlins have more chances to be executed than the last
)
  
  horde.seed(1234);

  horde.after(callback);
  window.onbeforeunload = stop;
  setTimeout(stop, ttl);
  horde.unleash();
}

describe('Monkey testing with gremlins ', function() {

  it('it should not raise any error', function() {
    browser.url('/');
    browser.click('button=Cerrar');

    browser.timeoutsAsyncScript(60000);
    browser.executeAsync(loadScript);

    browser.timeoutsAsyncScript(60000);
    browser.executeAsync(unleashGremlins, 50000);
  });

  afterAll(function() {
    browser.log('browser').value.forEach(function(log) {
      browser.logger.info(log.message.split(' ')[2]);
    });
  });

});